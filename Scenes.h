#pragma once
using namespace Structures;
class Scenes {
	
private:
	GlutContext * context;
	Drawer * drawer;
	void drawCarpet(Point2d startPoint, double size, int steps, double noise);
	void drawTriangle(Point2d a, double size, int step);
	bool pointInTriangle(Point2d &triangleStartPoint, double triangleSize, Point2d &testedPoint);
public:
	Scenes(GlutContext * context, Drawer * drawer) : context(context), drawer(drawer) {}
	void sierpinskiCarpet(Point2d & startPoint, double size, int steps, double noise);
	void sierpinskiTriangle_rec(Point2d &a, double size, int steps);
	void sierpinskiTriangle_chaos(Point2d &a, double size, unsigned long long diceRolls);
};
