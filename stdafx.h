
#pragma once
#define _USE_MATH_DEFINES
#include <stdlib.h>
#include <iostream>
#include <cmath>
#include <gl\glut.h>
#include <string>
#include <vector>
#include <functional>
#include <random>
#include <ctime>

#define light 1
#define egg 0
#define torus 1

#include "Axes.h"
#include "Structures.h"
#include "Color.h"
#include "Torus.h"
#include "GlutContext.h"
#include "Egg.h"
#include "Drawer.h"
#include "Scenes.h"
#include "TorusChain.h"

