#pragma once

#include "stdafx.h"

namespace Structures {
	struct Point2d {
		Point2d() : x(0), y(0) {}
		Point2d(double x, double y) : x(x), y(y) {}

		double x, y;
		//skala 0-360
		void rotate(double angle);
		void move(double x, double y);

		Point2d operator+(Point2d pt);
		Point2d operator-(Point2d pt);
	};

	struct Point3d {
		Point3d() : x(0), y(0), z(0) {}
		Point3d(double x, double y, double z) : x(x), y(y), z(z) {}
		Point3d(double coords[3]) : x(coords[0]), y(coords[1]), z(coords[2]) {}

		double x, y, z;

		Point3d operator+(Point3d pt);
		Point3d operator-(Point3d pt);
		double * toDoubleTab();

		void move(double x, double y, double z);
		void rotateX(double angle, Point3d center = Point3d(0, 0, 0));
		void rotateY(double angle, Point3d center = Point3d(0, 0, 0));
		void rotateZ(double angle, Point3d center = Point3d(0, 0, 0));

		void show();
	};

	struct Vector3d : Point3d {
		Vector3d() : Point3d() {}
		Vector3d(double x, double y, double z) : Point3d(x, y, z) {}
		Vector3d(double coords[3]) : Point3d(coords) {}
		void normalize();
	};
}


