#include "stdafx.h"
using namespace Structures;
class Torus {

public:
	std::vector<std::vector<Point3d>> points;

	typedef std::vector<std::vector<Vector3d>> NormalVectorsMatrix;
	NormalVectorsMatrix normalVectorsMatrix;

	Point3d center;
	double R;
	double r;
	Color * color;
	Color * colorBuffer;
	bool swapped = false;

	Torus(Point3d center, double R, double r, double divisor);
	Torus() {}
	std::vector<Point3d> & operator[](unsigned int index);

	unsigned int size();

	void draw(int from = -1, int to = -1);
	void rotate(Axes axis, double angle);
	void move(double x, double y, double z);
	void swapColors();

};