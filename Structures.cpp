
#include "stdafx.h"

void Structures::Point2d::rotate(double angle) {
	double angle_d = angle * 0.01745329;
	double sinus = sin(angle_d);
	double cosinus = cos(angle_d);

	double x = this->x, y = this->y;
	this->x = x * cosinus + y * sinus;
	this->y = x * -sinus + y * cosinus;
}

void Structures::Point2d::move(double x, double y) {
	this->x += x;
	this->y += y;
}

Structures::Point2d Structures::Point2d::operator+(Point2d pt) {
	Point2d out = { this->x + pt.x, this->y + pt.y };
	return out;
}

Structures::Point2d Structures::Point2d::operator-(Point2d pt) {
	Point2d out = {this->x - pt.x, this->y - pt.y};
	return out;
}

void Structures::Point3d::move(double x, double y, double z) {
	this->x += x;
	this->y += y;
	this->z += z;
}

Structures::Point3d Structures::Point3d::operator+(Point3d pt) {
	Point3d out = {this->x + pt.x, this->y + pt.y, this->z + pt.z};
	return out;
}

Structures::Point3d Structures::Point3d::operator-(Point3d pt) {
	Point3d out = {this->x - pt.x, this->y - pt.y, this->z - pt.z};
	return out;
}

double * Structures::Point3d::toDoubleTab() {
	double tab[3] = {this->x, this->y, this->z};
	return tab;
}

void Structures::Point3d::rotateX(double angle, Point3d center) {
	double y = this->y, z = this->z;

	this->y = center.y + (y - center.y)*cos(angle) - (z - center.z)*sin(angle);
	this->z = center.z + (z - center.z)*cos(angle) - (y - center.y)*sin(angle);
}
void Structures::Point3d::rotateY(double angle, Point3d center) {
	double x = this->x, z = this->z;

	this->x = center.x + (x - center.x)*cos(angle) - (z - center.z)*sin(angle);
	this->z = center.z + (z - center.z)*cos(angle) - (x - center.x)*sin(angle);
}
void Structures::Point3d::rotateZ(double angle, Point3d center) {
	double x = this->x, y = this->y;

	this->x = center.x + (x - center.x)*cos(angle) - (y - center.y)*sin(angle);
	this->y = center.y + (y - center.y)*cos(angle) + (x - center.x)*sin(angle);
}

void Structures::Point3d::show() {
	std::cout << "Point X: " << this->x << ", Y: " << this->y << ", Z: " << this->z << std::endl;
}

void Structures::Vector3d::normalize() {
	double lenght = sqrt(pow(x, 2) * pow(y, 2) * pow(z, 2));
	this->x /= lenght;
	this->y /= lenght;
	this->z /= lenght;
}

