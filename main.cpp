#include "stdafx.h"

//Wska�nik na kontekst programu
GlutContext * context = new GlutContext();
//Wska�nik na rysownika
Drawer * drawer = new Drawer();
//Wska�nik na obiekt scen
Scenes * scenes = new Scenes(context, drawer);


#if torus == 1
Point3d torusStartPoint = {-150, 0, 0};
Point3d viewerPoint = {0, 0, 400};
//TorusChain(Point3d &startPoint, double R, double r, double divisor, int torusChainLenght)
TorusChain torusChain(torusStartPoint, 25.0, 7.0, 20.0, 10);
int width = 1000;
int height = 1000;
int depth = 1000;
int windowWidth = 1000;
int windowHeight = 1000;
void scene() {
	context->clear();
	//drawer->draw3dAxes(GlutContext::height);
	torusChain.displayAll();
	context->flush();
}
void keyboard(unsigned char keyCode, int x, int y) {
	if (keyCode == '`') {
		context->switchUserInteractionStatus();
		torusChain.switchUserInteractionStatus();
	}
	torusChain.userInteractionKeyboardHandling(keyCode, x, y);
	scene();
}
void mouseStatic(int button, int state, int x, int y) {
	torusChain.userInteractionHandlingStatic(button, state, x, y);
	context->userRotatingHandlingStatic(button, state, x, y);
}
void mouseMotion(int x, int y) {
	torusChain.userInteractionHandlingMotion(x, y);
	context->userRotatingHandlingMotion(x, y);
	scene();
}
#if light == 1
void lightParameters() {
	torusChain.setLightParameters();
}
#endif
#endif // torus


#if egg == 1
Point3d eggStartPoint = {0, -5, 0};
Point3d viewerPoint = {0, 0, 25};
int width = 200;
int height = 200;
int depth = 200;
int windowWidth = 800;
int windowHeight = 800;
EggScene eggScene = EggScene(eggStartPoint, 120, 1);

void scene() {
	context->userInteractionEnabled = false;
	eggScene.userInteractionStatus = true;
	context->clear();
	//drawer->draw3dAxes(GlutContext::height);
	eggScene.mainScene();
	context->flush();
}
void keyboard(unsigned char keyCode, int x, int y) {
	eggScene.keyboardHandler(keyCode, x, y);
	scene();
}
void mouseStatic(int button, int state, int x, int y) {
	eggScene.userInteractionHandlingStatic(button, state, x, y);
	context->userRotatingHandlingStatic(button, state, x, y);
}
void mouseMotion(int x, int y) {
	eggScene.userInteractionHandlingMotion(x, y);
	context->userRotatingHandlingMotion(x, y);
	scene();
}
#if light == 1
void lightParameters() {
	eggScene.setLightParameters();
}
#endif
#endif // egg


int main() {
	context->setMainLoopFunction(scene);
	context->setKeyboardListenerFunction(keyboard);
	context->setMouseListenerFunction(mouseStatic);
	context->setMouseMotionListenerFunction(mouseMotion);
	context->setBackgroundColor(Color(178, 201, 209));
#if light == 1
	context->setBackgroundColor(Color::BLACK);
	#ifdef egg == 1
	context->setLightSpecifiers(lightParameters);
	#endif
	#ifdef torus == 1
	//context->setLightSpecifiers(lightParameters);
	#endif
#else
	context->setBackgroundColor(Color(178, 201, 209));
#endif
	context->viewerPoint = viewerPoint;
	context->init3d("Pocz�tek", windowWidth, windowHeight, width, height, depth);
    return 0;
}

