#pragma once
#include "stdafx.h"

using namespace Structures;

class TorusChain {
public:







	#if light == 1
	// Definicja materia�u jajka
	// wsp�czynniki ka =[kar,kag,kab] dla �wiat�a otoczenia
	float materialAmbient[4] = {0.2f, 0.2f, 0.2f, 0.2f};
	// wsp�czynniki kd =[kdr,kdg,kdb] �wiat�a rozproszonego
	float materialDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	// wsp�czynniki ks =[ksr,ksg,ksb] dla �wiat�a odbitego   
	float materialSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	// wsp�czynnik n opisuj�cy po�ysk powierzchni
	float materialShininess = {120.0f};


	// po�o�enie �r�d�a
	float firstLightPosition[4] = {300.0f, 300.0f, 0.0f, 1.0f};
	float firstLightAzimuth = 0;
	float firstLightElevation = M_PI_4;
	// sk�adowe intensywno�ci �wiecenia �r�d�a �wiat�a otoczenia
	// Ia = [Iar,Iag,Iab]
	float firstLightAmbient[4] = {0.0f, 0.0f, 400.0f, 1.0f};

	// sk�adowe intensywno�ci �wiecenia �r�d�a �wiat�a powoduj�cego
	// odbicie dyfuzyjne Id = [Idr,Idg,Idb]
	float firstLightDiffuse[4] = {0.0f, 0.0f, 400.0f, 1.0f};

	// sk�adowe intensywno�ci �wiecenia �r�d�a �wiat�a powoduj�cego
	// odbicie kierunkowe Is = [Isr,Isg,Isb]
	float firstLightSpecular[4] = {1.0f, 1.0f, 400.0f, 1.0f};

	float secondLightPosition[4] = {-300.0f, 300.0f, 0.0f, 1.0f}; // pozycja
	float secondLightAzimuth = M_PI_2;
	float secondLightElevation = M_PI_4;

	float secondLightAmbient[4] = {400.0f, 0.0f, 0.0f, 1.0f}; // warto�ci sk�adowych RGBA �wiat�a otaczaj�cego
	float secondLightDiffuse[4] = {400.0f, 0.0f, 0.0f, 1.0f}; // warto�ci sk�adowych RGBA �wiat�a rozproszonego
	float secondLightSpecular[4] = {400.0f, 56.0f, 0.0f, 1.0f}; // warto�ci sk�adowych RGBA �wiat�a odbitego
															  // sk�adowa sta�a ds dla modelu zmian o�wietlenia w funkcji
															  // odleg�o�ci od �r�d�a
	float attributeConstant = {1.0f};

	// sk�adowa liniowa dl dla modelu zmian o�wietlenia w funkcji
	// odleg�o�ci od �r�d�a
	float attributeLinear = {0.05f};

	// sk�adowa kwadratowa dq dla modelu zmian o�wietlenia w funkcji
	// odleg�o�ci od �r�d�a
	float attributeQuadratic = {0.001f};
	#endif


	Point3d startPoint;
	int torusChainLenght = 30;
	double R, r, divisor;

	TorusChain(Point3d &startPoint, double R, double r, double divisor, int torusChainLenght = 10);
	void userInteractionHandlingStatic(int button, int state, int x, int y);
	void userInteractionHandlingMotion(int x, int y);
	void userInteractionKeyboardHandling(unsigned char keyCode, int x, int y);

	double actualMouseX = 0;
	double actualMouseY = 0;

	double previousMouseX = 0;
	double previousMouseY = 0;

	bool leftMouseButtonPressed;
	bool rightMouseButtonPressed;

	bool userInteractionStatus = false;

	void switchUserInteractionStatus();

	Torus * chosenTorus;
	int chosenTorusIndex = 0;
	std::vector<Torus> toruses;

	void displayAll();
	#if light == 1
	void updateAngle(float &angle, float delta);
	void setLightParameters();
	#endif
};
