
#include "stdafx.h"

int GlutContext::width = 100;
int GlutContext::height = 100;
int GlutContext::depth = 0;

Point3d GlutContext::viewerPoint = Point3d(0, 0, 0);
Point3d GlutContext::cameraVector = Point3d(0.0, 1.0, 0.0);
Point3d GlutContext::observedPoint = Point3d(0, 0, 0);

double GlutContext::azimuth = 0;
double GlutContext::elevation = 0;
double GlutContext::viewerVectorLength = 0;

double GlutContext::actualMouseX = 0;
double GlutContext::actualMouseY = 0;

double GlutContext::previousMouseX = 0;
double GlutContext::previousMouseY = 0;

bool GlutContext::leftMouseButtonPressed = false;
bool GlutContext::rightMouseButtonPressed = false;

bool GlutContext::userInteractionEnabled = true;

Color GlutContext::backgroundColor = Color::BLACK;

void (*GlutContext::mainLoopFunctionPointer)() = nullptr;
void (*GlutContext::keyboardFunctionPointer)(unsigned char, int, int) = nullptr;
void (*GlutContext::mouseFunctionPointer)(int button, int state, int x, int y) = nullptr;
void (*GlutContext::mouseMotionFunctionPointer)(int x, int y) = nullptr;
#ifdef light == 1
void (*GlutContext::lightSpecifiers)() = nullptr;
#endif

GlutContext::GlutContext() {
}

void GlutContext::init2d(std::string windowName, int width, int height, bool swapBuffers) {
	GlutContext::width = width;
	GlutContext::height = height;

	if (swapBuffers) {
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	} else {
		glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	}

	//Ustawienie pocz�tkowego rozmiaru okna
	glutInitWindowSize(GlutContext::width, GlutContext::height);

	//Utworzenie okna
	glutCreateWindow(windowName.c_str());

	//Funkcja rysuj�ca zawarto�� onka
	glutDisplayFunc(mainLoopFunctionPointer); 
	glutKeyboardFunc(GlutContext::keyboardFunctionPointer);
	glutMouseFunc(GlutContext::mouseFunctionPointer);

	// Za�adowanie funkcji reakcji na zmian� rozmiaru okna
	glutReshapeFunc(GlutContext::changeSize2d);

	glClearColor(backgroundColor.red, backgroundColor.green, backgroundColor.blue, 1.0f);
	if (swapBuffers) {
		glutSwapBuffers();
	}
	//Inicjalizacja p�tli g��wnej gluta
	glutMainLoop(); 
}

void GlutContext::init3d(std::string windowName, int windowWidth, int windowHeight, int width, int height, int depth, bool swapBuffers) {

	calculateObserverVectorLength();
	calculateAzimuthAndElevation();

	GlutContext::width = width;
	GlutContext::height = height;
	GlutContext::depth = depth;

	if (swapBuffers) {
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	} else {
		glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH);
	}

	//Ustawienie pocz�tkowego rozmiaru okna
	glutInitWindowSize(windowWidth, windowHeight);
	//Utworzenie okna
	glutCreateWindow(windowName.c_str());
	//Funkcja rysuj�ca zawarto�� onka
	glutDisplayFunc(GlutContext::mainLoopFunctionPointer);

	glutKeyboardFunc(GlutContext::keyboardFunctionPointer);
	glutMouseFunc(GlutContext::mouseFunctionPointer);
	glutMotionFunc(GlutContext::mouseMotionFunctionPointer);

	// Za�adowanie funkcji reakcji na zmian� rozmiaru okna
	glutReshapeFunc(GlutContext::changeSize3d);

	glClearColor(backgroundColor.red, backgroundColor.green, backgroundColor.blue, 1.0f);
	if (swapBuffers) {
		glutSwapBuffers();
	}

#ifdef light == 1
	if (lightSpecifiers != nullptr) {
		lightSpecifiers();
	}
#endif // light == 1

	glEnable(GL_DEPTH_TEST);
	//Inicjalizacja p�tli g��wnej gluta
	glutMainLoop();
}

void GlutContext::changeWindowTitle(std::string title) {
	glutSetWindowTitle(title.c_str());
}

void GlutContext::switchUserInteractionStatus() {
	userInteractionEnabled = !userInteractionEnabled;
	if (userInteractionEnabled) {
		changeWindowTitle("Kamera aktywna.");
	} else {
		changeWindowTitle("Kamera zablokowana!");
	}

}

void GlutContext::clear() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//gluLookAt(GLdouble eyeX, GLdouble eyeY, GLdouble eyeZ, // wsp�rz�dne oka obserwatora
			  //GLdouble centerX, GLdouble centerY, GLdouble centerZ, // punkt na kt�ry obserwator patrzy
			  //GLdouble upX, GLdouble upY, GLdouble upZ); //wektor pionu kamery
	calculatePosition();
	glLoadIdentity();
	gluLookAt(viewerPoint.x, viewerPoint.y, viewerPoint.z, 
			  observedPoint.x, observedPoint.y, observedPoint.z,
			  cameraVector.x, cos(elevation), cameraVector.z);
}

void GlutContext::userRotatingHandlingStatic(int button, int state, int x, int y) {
	//Sprawdzany jest status obs�ugi u�ytkownika
	if (!userInteractionEnabled) {
		return;
	}
	//Sprawdzany jest status poszczeg�lnych klawiszy myszy
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		leftMouseButtonPressed = true;
	} else {
		leftMouseButtonPressed = false;
	}
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		rightMouseButtonPressed = true;
	} else {
		rightMouseButtonPressed = false;
	}

	//Aktualizowane s� zmienne
	previousMouseX = actualMouseX;
	previousMouseY = actualMouseY;

	actualMouseX = x;
	actualMouseY = y;
}

//Obs�uga poruszania si� za pomoc� myszy
void GlutContext::userRotatingHandlingMotion(int x, int y) {
	if (!userInteractionEnabled) {
		return;
	}

	previousMouseX = actualMouseX;
	previousMouseY = actualMouseY;

	actualMouseX = x;
	actualMouseY = y;
	//Odpowiednie ruchy w zale�no�ci od wci�ni�tego przycisku
	if (leftMouseButtonPressed) {
		azimuth -= (previousMouseX - actualMouseX) / 100;
		elevation -= (previousMouseY - actualMouseY) / 100;
	} else if (rightMouseButtonPressed) {
		viewerVectorLength -= (previousMouseY - actualMouseY);
	}
}

void GlutContext::flush() {
	glFlush();
}

void GlutContext::setDrawingColor(Color color) {
	glColor3d(color.red, color.green, color.blue);
}

void GlutContext::setDrawingColor(double rgb[3]) {
	glColor3d(rgb[0], rgb[1], rgb[2]);
}

void GlutContext::setDrawingColor(int rgb[3]) {
	glColor3d(rgb[0]/(double)255, rgb[1] / (double)255, rgb[2] / (double)255);
}

// Parametry horizontal i vertical (szeroko�� i wysoko�� okna) s�
// przekazywane do funkcji za ka�dym razem, gdy zmieni si� rozmiar okna
void GlutContext::changeSize2d(GLsizei width, GLsizei height) {
	// Ustawienie wielko�ciokna okna urz�dzenia (Viewport)
	// W tym przypadku od (0,0) do (horizontal, vertical)
	glViewport(0, 0, width, height);

	// Okre�lenie uk�adu wsp�rz�dnych obserwatora
	glMatrixMode(GL_PROJECTION);

	// Okre�lenie przestrzeni ograniczaj�cej
	glLoadIdentity();

	// Wyznaczenie wsp�czynnika proporcji okna
	GLfloat screenRatio = (GLfloat)width / (GLfloat)height;
	
	double secondPartOfWidth = GlutContext::width / 2;
	double secondPartOfHeight = GlutContext::height / 2;

	if (height == 0) height = 1;
	// Gdy okno na ekranie nie jest kwadratem wymagane jest okre�lenie okna obserwatora.
	// Pozwala to zachowa� w�a�ciwe proporcje rysowanego obiektu.
	// Do okre�lenia okna obserwatora s�u�y funkcja glOrtho(...)
	if (width <= height) {
		glOrtho(-secondPartOfWidth, secondPartOfWidth, -secondPartOfHeight / screenRatio, secondPartOfHeight / screenRatio, 1.0, -1.0);
	}
	else {
		glOrtho(-secondPartOfWidth * screenRatio, secondPartOfWidth * screenRatio, -secondPartOfHeight, secondPartOfHeight, 1.0, -1.0);
	}

	// Okre�lenie uk�adu wsp�rz�dnych    
	glMatrixMode(GL_MODELVIEW);
	// Okre�lenie przestrzeni ograniczaj�cej
	glLoadIdentity();
}

void GlutContext::changeSize3d(GLsizei width, GLsizei height) {

	// Okre�lenie uk�adu wsp�rz�dnych obserwatora
	glMatrixMode(GL_PROJECTION);

	// Okre�lenie przestrzeni ograniczaj�cej
	glLoadIdentity();

	double secondPartOfWidth = GlutContext::width / 2;
	double secondPartOfHeight = GlutContext::height / 2;
	double secondPartOfDepth = GlutContext::depth / 2;

	// Wyznaczenie wsp�czynnika proporcji okna
	if (height == 0) height = 1;
	GLfloat screenRatio = (GLfloat) width / (GLfloat) height;

	// Funkcja okre�laj�ca bry�� renderowania : void gluPerspective(GLdouble fovy, GLdouble aspect, GLdouble zNear, GLdouble zFar)
	gluPerspective(50, 1, 1, GlutContext::depth * 10);

	// Ustawienie wielko�ciokna okna urz�dzenia (Viewport)
	// W tym przypadku od (0,0) do (horizontal, vertical)
	if (width <= height) {
		glViewport(0, (height - width) / 2, width, width);
	} else {
		glViewport((width - height) / 2, 0, height, height);
	}

	// Okre�lenie uk�adu wsp�rz�dnych    
	glMatrixMode(GL_MODELVIEW);
}

void GlutContext::setKeyboardListenerFunction(void(*function)(unsigned char keyCode, int x, int y)) {
	GlutContext::keyboardFunctionPointer = function;
}

void GlutContext::setMainLoopFunction(void(*function)()) {
	GlutContext::mainLoopFunctionPointer = function;
}

void GlutContext::setMouseListenerFunction(void(*function)(int button, int state, int x, int y)) {
	GlutContext::mouseFunctionPointer = function;
}

void GlutContext::setMouseMotionListenerFunction(void(*function)(int x, int y)) {
	mouseMotionFunctionPointer = function;
}

void GlutContext::changeMainLoopFunction(void(*function)()) {
	GlutContext::mainLoopFunctionPointer = function;
	glutDisplayFunc(function);
}

void GlutContext::rotateObserverPoint(char direction, double angle, char direction2, double angle2) {

	switch (direction) {
		case 'L': {
			azimuth -= angle;
			break;
		}
		case 'R': {
			azimuth += angle;
			break;
		}
		case 'U': {
			elevation += angle;
			break;
		}
		case 'D': {
			elevation -= angle;
			break;
		}
		default:
			break;
	}

	if (direction2 != -1) {
		switch (direction2) {
			case 'L': {
				azimuth -= angle2;
				break;
			}
			case 'R': {
				azimuth += angle2;
				break;
			}
			case 'U': {
				elevation += angle2;
				break;
			}
			case 'D': {
				elevation -= angle2;
				break;
			}
			default:
				break;
		}
	}

	if (azimuth > 2 * M_PI) {
		azimuth = azimuth - 2 * M_PI;
	} else if (azimuth < 0) {
		azimuth = 2 * M_PI - azimuth;
	}

	if (elevation > 2 * M_PI) {
		elevation = elevation - 2 * M_PI;
	} else if (elevation < 0) {
		elevation = 2 * M_PI - elevation;
	}

}

void GlutContext::calculateObserverVectorLength() {
	viewerVectorLength = sqrt(viewerPoint.x*viewerPoint.x + viewerPoint.y*viewerPoint.y + viewerPoint.z*viewerPoint.z);
}

//TODO P�ki co zrobione na szytwno, ale jeszcze musz� to rozpracowa�
void GlutContext::calculateAzimuthAndElevation() {
	//double cosElevation = pow(viewerPoint.x / viewerVectorLength, 2) + pow(viewerPoint.z / viewerVectorLength, 2);
	//double cosAsimuth = abs(viewerPoint.x) / sqrt(pow(viewerPoint.x, 2) + pow(viewerPoint.z, 2));

	elevation = 0;//acos(cosElevation);
	azimuth = M_PI_2;// acos(cosAsimuth);
}

void GlutContext::calculatePosition() {
	viewerPoint.x = viewerVectorLength * cos(azimuth) * cos(elevation);
	viewerPoint.y = viewerVectorLength * sin(elevation);
	viewerPoint.z = viewerVectorLength * sin(azimuth) * cos(elevation);
}

//Kolor wn�trza okna
// (red, green, blue, alpha)
void GlutContext::setBackgroundColor(Color color) {
	GlutContext::backgroundColor = color;
}
void GlutContext::setBackgroundColor(int rgb[3]) {
	GlutContext::backgroundColor = Color(rgb);
}
void GlutContext::setBackgroundColor(double rgb[3]) {
	GlutContext::backgroundColor = Color(rgb);
}

#ifdef light == 1
void GlutContext::setLightSpecifiers(void(*function)()) {
	lightSpecifiers = function;
}
#endif
