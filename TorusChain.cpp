#include "stdafx.h"

using namespace Structures;

TorusChain::TorusChain(Point3d &startPoint, double R, double r, double divisor, int torusChainLenght) : startPoint(startPoint), R(R), r(r), divisor(divisor), torusChainLenght(torusChainLenght) {
	//Generowanie poszczeg�lnych torus�w
	this->toruses = std::vector<Torus>(torusChainLenght);
	Point3d center = startPoint;
	for (int i = 0; i < torusChainLenght; i++) {
		toruses[i] = Torus(center, R, r, divisor);
		if (i % 2 == 0) {
			toruses[i].rotate(X, M_PI / 2);
		}
		center.move(R + r, 0, 0);
	}
	chosenTorus = &toruses[0];
	chosenTorus->swapColors();
}

void TorusChain::displayAll() {
	//Wy�wietlenie tylnich po��wek poziomcyh torus�w
	for (int i = 0; i < torusChainLenght; i += 2) {
		toruses[i].draw(0, toruses[i].size() / 2);
	}
	//Wy�wietlenie pe�nych pionowych torus�w
	for (int i = 1; i < torusChainLenght; i += 2) {
		toruses[i].draw();
	}
	//Wy�wietlenie przednich po��wek poziomcyh torus�w
	for (int i = 0; i < torusChainLenght; i += 2) {
		toruses[i].draw(toruses[i].size() / 2, toruses[i].size());
	}
}

#if light == 1
void TorusChain::updateAngle(float & angle, float delta) {
	angle += delta;

	if (angle < 0) {
		angle += 2 * M_PI;
	}

	if (angle > 2 * M_PI) {
		angle = angle - 2 * M_PI;
	}
}

void TorusChain::setLightParameters() {
	// Ustawienie patrametr�w materia�u
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glMaterialfv(GL_FRONT, GL_SPECULAR, this->materialSpecular);
	glMaterialfv(GL_FRONT, GL_AMBIENT, this->materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, this->materialDiffuse);
	glMaterialf(GL_FRONT, GL_SHININESS, this->materialShininess);

	// Ustawienie parametr�w �r�d�a nr 0
	glLightfv(GL_LIGHT0, GL_AMBIENT, this->firstLightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, this->firstLightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, this->firstLightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, this->firstLightPosition);

	glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, this->attributeConstant);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, this->attributeLinear);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, this->attributeQuadratic);

	// Ustawienie parametr�w �r�d�a nr 1
	glLightfv(GL_LIGHT1, GL_AMBIENT, this->secondLightAmbient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, this->secondLightDiffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, this->secondLightSpecular);
	glLightfv(GL_LIGHT1, GL_POSITION, this->secondLightPosition);

	glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, this->attributeConstant);
	glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, this->attributeLinear);
	glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, this->attributeQuadratic);

	// Ustawienie opcji systemu o�wietlania sceny
	glShadeModel(GL_SMOOTH); // w�aczenie �agodnego cieniowania
	glEnable(GL_LIGHTING);   // w�aczenie systemu o�wietlenia sceny
	glEnable(GL_LIGHT0);     // w��czenie �r�d�a o numerze 0
	glEnable(GL_LIGHT1);     // w��czenie �r�d�a o numerze 1
}
#endif // light == 1
void TorusChain::userInteractionKeyboardHandling(unsigned char keyCode, int x, int y) {
	//Funckja kt�ra obs�uguje wyb�r aktualnie przemieszczanego torusa
	bool changeMade = false;
	if (keyCode == 'a' && chosenTorusIndex > 0) {
		chosenTorusIndex--;
		changeMade = true;
	} else if (keyCode == 'd' && chosenTorusIndex < torusChainLenght - 1) {
		chosenTorusIndex++;
		changeMade = true;
	}
	if (changeMade) {
		chosenTorus->swapColors();
		chosenTorus = &toruses[chosenTorusIndex];
		chosenTorus->swapColors();
	}
}


void TorusChain::userInteractionHandlingStatic(int button, int state, int x, int y) {
	//Funckja wykrywaj�ca przyci�ni�cie klawiszy myszy
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		leftMouseButtonPressed = true;
	} else {
		leftMouseButtonPressed = false;
	}

	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		rightMouseButtonPressed = true;
	} else {
		rightMouseButtonPressed = false;
	}

	previousMouseX = actualMouseX;
	previousMouseY = actualMouseY;
	actualMouseX = x;
	actualMouseY = y;
}

void TorusChain::userInteractionHandlingMotion(int x, int y) {
	//Funckja reaguj�ca na przesuni�cie myszy na ekranie
	if (!userInteractionStatus) {
		return;
	}
	previousMouseX = actualMouseX;
	previousMouseY = actualMouseY;
	actualMouseX = x;
	actualMouseY = y;

	//Je�eli kt�ry� z przycisk�w zosta� wci�ni�ty, zostaj� wykonane odpowiednie akcje
	//Przesuni�cie torusa
	if (leftMouseButtonPressed) {
		#if light == 1
		double vectorLenght = sqrt(pow(this->firstLightPosition[0], 2) + pow(this->firstLightPosition[1], 2) + pow(this->firstLightPosition[2], 2));
		updateAngle(firstLightAzimuth, (previousMouseX - actualMouseX) / 100);
		updateAngle(firstLightElevation, (previousMouseY - actualMouseY) / 100);

		this->firstLightPosition[0] = vectorLenght * cos(firstLightAzimuth) * cos(firstLightElevation);
		this->firstLightPosition[1] = vectorLenght * sin(firstLightElevation);
		this->firstLightPosition[2] = vectorLenght * sin(firstLightAzimuth) * cos(firstLightElevation);

		glLightfv(GL_LIGHT0, GL_POSITION, this->firstLightPosition);
		#else
		chosenTorus->move(0, (previousMouseY - actualMouseY) / 50, 0);
		chosenTorus->move(-(previousMouseX - actualMouseX) / 50, 0, 0);
		#endif
	}

	//Obr�t
	if (rightMouseButtonPressed) {
		#if light == 1
		double vectorLenght = sqrt(pow(this->secondLightPosition[0], 2) + pow(this->secondLightPosition[1], 2) + pow(this->secondLightPosition[2], 2));
		updateAngle(secondLightAzimuth, (previousMouseX - actualMouseX) / 100);
		updateAngle(secondLightElevation, (previousMouseY - actualMouseY) / 100);

		this->secondLightPosition[0] = vectorLenght * cos(secondLightAzimuth) * cos(secondLightElevation);
		this->secondLightPosition[1] = vectorLenght * sin(secondLightElevation);
		this->secondLightPosition[2] = vectorLenght * sin(secondLightAzimuth) * cos(secondLightElevation);

		glLightfv(GL_LIGHT1, GL_POSITION, this->secondLightPosition);
		#else
		chosenTorus->rotate(Z, (previousMouseY - actualMouseY) > 0 ? 0.01 : -0.01);
		#endif
	}
}

void TorusChain::switchUserInteractionStatus() {
	//zmiana statusu obs�ugi u�ytkownika
	userInteractionStatus = !userInteractionStatus;
}



