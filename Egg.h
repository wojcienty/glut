#pragma once
#include "stdafx.h"
using namespace Structures;
class EggScene {
private:
	double size;
	double divisor;

	typedef std::vector<std::vector<Point3d>> PointMatrix;
	PointMatrix pointsMatrix;

	typedef std::vector<std::vector<Color>> ColorMatrix;
	ColorMatrix colorMatrix;

	Point3d startPoint;
	char choice = '3';
	bool reset = true;

	#if light == 1

	double actualMouseX = 0;
	double actualMouseY = 0;

	double previousMouseX = 0;
	double previousMouseY = 0;

	bool leftMouseButtonPressed;
	bool rightMouseButtonPressed;

	typedef std::vector<std::vector<Vector3d>> NormalVectorsMatrix;
	NormalVectorsMatrix normalVectorsMatrix;

	// Definicja materia�u jajka
	// wsp�czynniki ka =[kar,kag,kab] dla �wiat�a otoczenia
	float materialAmbient[4] = {0.2f, 0.2f, 0.2f, 0.2f};
	// wsp�czynniki kd =[kdr,kdg,kdb] �wiat�a rozproszonego
	float materialDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	// wsp�czynniki ks =[ksr,ksg,ksb] dla �wiat�a odbitego   
	float materialSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	// wsp�czynnik n opisuj�cy po�ysk powierzchni
	float materialShininess = {80.0f};
	

	// po�o�enie �r�d�a
	float firstLightPosition[4] = {15.0f, 15.0f, 0.0f, 1.0f};
	float firstLightAzimuth = 0;
	float firstLightElevation = M_PI_4;
	// sk�adowe intensywno�ci �wiecenia �r�d�a �wiat�a otoczenia
	// Ia = [Iar,Iag,Iab]
	float firstLightAmbient[4] = {0.0f, 0.0f, 1.0f, 1.0f};
	
	// sk�adowe intensywno�ci �wiecenia �r�d�a �wiat�a powoduj�cego
	// odbicie dyfuzyjne Id = [Idr,Idg,Idb]
	float firstLightDiffuse[4] = {0.0f, 0.0f, 1.0f, 1.0f};
	
	// sk�adowe intensywno�ci �wiecenia �r�d�a �wiat�a powoduj�cego
	// odbicie kierunkowe Is = [Isr,Isg,Isb]
	float firstLightSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};

	float secondLightPosition[4] = {-15.0f, 15.0f, 0.0f, 1.0f}; // pozycja
	float secondLightAzimuth = M_PI_2;
	float secondLightElevation = M_PI_4;

	float secondLightAmbient[4] = {1.0f, 0.0f, 0.0f, 1.0f}; // warto�ci sk�adowych RGBA �wiat�a otaczaj�cego
	float secondLightDiffuse[4] = {1.0f, 0.0f, 0.0f, 1.0f}; // warto�ci sk�adowych RGBA �wiat�a rozproszonego
	float secondLightSpecular[4] = {1.0f, 0.56f, 0.0f, 1.0f}; // warto�ci sk�adowych RGBA �wiat�a odbitego
	// sk�adowa sta�a ds dla modelu zmian o�wietlenia w funkcji
	// odleg�o�ci od �r�d�a
	float attributeConstant = {1.0f};
	
	// sk�adowa liniowa dl dla modelu zmian o�wietlenia w funkcji
	// odleg�o�ci od �r�d�a
	float attributeLinear = {0.05f};

	// sk�adowa kwadratowa dq dla modelu zmian o�wietlenia w funkcji
	// odleg�o�ci od �r�d�a
	float attributeQuadratic = {0.001f};
	#endif
	



	void drawVertex3d(Point3d p1, Point3d p2, Point3d p3, Point3d p4);
	#if light == 1
	void drawLitTriangle(Point3d p1, Point3d p2, Point3d p3, Point3d p4, Vector3d c1, Vector3d c2, Vector3d c3, Vector3d c4);
	#else
	void drawColorfulTriangle(Point3d p1, Point3d p2, Point3d p3, Point3d p4, Color c1, Color c2, Color c3, Color c4);
	#endif

public:
	EggScene(Point3d startPoint, double divisor = 100, double size = 1);
	bool userInteractionStatus = false;
	void mainScene();
	void keyboardHandler(unsigned char keyCode, int x, int y);
	void mouseHandler(int button, int state, int x, int y);

	void drawMesh();
	void drawPoints();
	void drawTriangles();
	void drawPolygons();
	void switchUserInteractionStatus();
#if light == 1
	void userInteractionHandlingStatic(int button, int state, int x, int y);
	void userInteractionHandlingMotion(int x, int y);
	void updateAngle(float &angle, float delta);
	void setLightParameters();
#endif
};
