#include "stdafx.h"

Torus::Torus(Point3d center, double R, double r, double divisor) {
	this->center = center;
	this->R = R;
	this->r = r;
	#if light == 0
	std::random_device rd;
	std::mt19937_64 mt(rd());
	std::uniform_real_distribution<double> dis(0, 1);
	this->color = new Color(dis(mt), dis(mt), dis(mt));
	this->colorBuffer = new Color(this->color->red, this->color->green, this->color->blue);
	#endif
	points = std::vector<std::vector<Point3d>>(divisor);
	normalVectorsMatrix = NormalVectorsMatrix(divisor);
	for (int i = 0; i < divisor; i++) {
		points[i] = std::vector<Point3d>(divisor);
		#if light == 1
		normalVectorsMatrix[i] = std::vector<Vector3d>(divisor);
		#endif
	}

	//Tworzenie macierzy punkt�w wed�ug podanego wzoru
	double u, v, x, y, z;
	double PIx2 = 2 * M_PI;
	for (int row = 0; row < divisor; row++) {
		for (int col = 0; col < divisor; col++) {
			u = col / divisor;
			v = row / divisor;
			x = (R + r * cos(PIx2 * v)) * cos(PIx2 * u);
			y = (R + r * cos(PIx2 * v)) * sin(PIx2 * u);
			z = r * sin(PIx2 * v);

			points[row][col] = Point3d(center.x + x, center.y + y, center.z + z);


			#if light == 1
			double x_u = -2 * M_PI * sin(2 * M_PI * u) * (r * cos(2 * M_PI * v) + R);
			double x_v = -2 * M_PI * r * cos(2 * M_PI * u) * sin(2 * M_PI * v);

			double y_u = 2 * M_PI * cos(2 * M_PI * u) * (r * cos(2 * M_PI * v) + R);
			double y_v = -2 * M_PI * r * sin(2 * M_PI * u) * sin(2 * M_PI * v);

			double z_u = 0;
			double z_v = 2 * M_PI * r * cos(2 * M_PI * v);

			double nx = y_u * z_v - z_u * y_v;
			double ny = z_u * x_v - x_u * z_v;
			double nz = x_u * y_v - y_u * x_v;

			double lenghtOfNormalVector = sqrt(pow(nx, 2) + pow(ny, 2) + pow(nz, 2));
			nx /= lenghtOfNormalVector;
			ny /= lenghtOfNormalVector;
			nz /= lenghtOfNormalVector;

			normalVectorsMatrix[row][col] = Vector3d(nx, ny, nz);
			#endif
		}
	}

}

std::vector<Point3d> & Torus::operator[](unsigned int index) {
	return points[index];
}

unsigned int Torus::size() {
	return points.size();
}

void Torus::draw(int from, int to) {
	if (from < 0 || to < 0) {
		from = 0;
		to = size();
	}

	//Kolumny to poprzeczne przekroje 
	//
	//	p4 *   p1 *	
	//
	//	p3 *   p2 *	
	//
	Point3d p1, p2, p3, p4;
	#if light == 1
	Vector3d v1, v2, v3, v4;
	#endif
	glColor3d(0, 0, 0);
	int pointsAmount = size();
	for (int col = from; col < to; col++) {
		for (int row = 0; row < pointsAmount; row++) {
			p1 = points[row][col];
			p2 = points[(row + 1) % pointsAmount][col];
			p3 = points[(row + 1) % pointsAmount][(col + 1) % pointsAmount];
			p4 = points[row][(col + 1) % pointsAmount];
			#if light == 0
			glColor3d(this->color->red, this->color->green, this->color->blue);
			glBegin(GL_POLYGON);
			glVertex3d(p3.x, p3.y, p3.z);
			glVertex3d(p4.x, p4.y, p4.z);
			glVertex3d(p1.x, p1.y, p1.z);
			glVertex3d(p2.x, p2.y, p2.z);
			glEnd();
			glLineWidth(2);
			glBegin(GL_LINE_LOOP);
			glVertex3d(p3.x, p3.y, p3.z);
			glVertex3d(p4.x, p4.y, p4.z);
			glVertex3d(p1.x, p1.y, p1.z);
			glVertex3d(p2.x, p2.y, p2.z);
			glEnd();
			glLineWidth(1);
			#else
			v1 = normalVectorsMatrix[row][col];
			v2 = normalVectorsMatrix[(row + 1) % pointsAmount][col];
			v3 = normalVectorsMatrix[(row + 1) % pointsAmount][(col + 1) % pointsAmount];
			v4 = normalVectorsMatrix[row][(col + 1) % pointsAmount];
			glBegin(GL_POLYGON);

			glNormal3d(v3.x, v3.y, v3.z);
			glVertex3d(p3.x, p3.y, p3.z);

			glNormal3d(v4.x, v4.y, v4.z);
			glVertex3d(p4.x, p4.y, p4.z);

			glNormal3d(v1.x, v1.y, v1.z);
			glVertex3d(p1.x, p1.y, p1.z);

			glNormal3d(v2.x, v2.y, v2.z);
			glVertex3d(p2.x, p2.y, p2.z);

			glEnd();
			#endif
		}
	}

}

void Torus::rotate(Axes axis, double angle) {
	for (int row = 0; row < size(); row++) {
		for (int col = 0; col < size(); col++) {
			switch (axis) {
				case X: this->points[row][col].rotateX(angle); break;
				case Y: this->points[row][col].rotateY(angle); break;
				case Z: this->points[row][col].rotateZ(angle, this->center); break;
				default: break;
			}
		}
	}
}

void Torus::swapColors() {
	if (swapped) {
		delete this->color;
		this->color = colorBuffer;
	} else {
		this->color = new Color(1, 1, 1);
	}
	swapped = !swapped;
}

void Torus::move(double x, double y, double z) {
	for (int row = 0; row < size(); row++) {
		for (int col = 0; col < size(); col++) {
			this->points[row][col].move(x, y, z);
		}
	}
	center.move(x, y, z);
}