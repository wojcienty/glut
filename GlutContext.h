#pragma once

#include "stdafx.h"

using namespace Structures;
class GlutContext {

private: 

	static void(*keyboardFunctionPointer)(unsigned char keyCode, int x, int y);
	static void(*mouseFunctionPointer)(int button, int state, int x, int y);
	static void(*mainLoopFunctionPointer)();
	static void(*mouseMotionFunctionPointer)(int x, int y);
#ifdef light == 1
	static void(*lightSpecifiers)();
#endif
	static Color backgroundColor;

	static double actualMouseX;
	static double actualMouseY;

	static double previousMouseX;
	static double previousMouseY;

	static bool leftMouseButtonPressed;
	static bool rightMouseButtonPressed;

	
	
public:
	GlutContext();
	static bool userInteractionEnabled;
	static int width, height, depth;
	static double azimuth;
	static double elevation;
	static double viewerVectorLength;


	static Point3d viewerPoint;
	static Point3d cameraVector;
	static Point3d observedPoint;

	static void init2d(std::string windowName, int width = 300, int height = 300, bool swapBuffers = false);
	static void init3d(std::string windowName, int windowWidth = 500, int windowHeight = 500, int width = 300, int height = 300, int depth = 300, bool swapBuffers = false);
	static void clear();
	static void flush();

	static void setBackgroundColor(Color color);
	static void setBackgroundColor(int rgb[3]);
	static void setBackgroundColor(double rgb[3]);

	static void setDrawingColor(int rgb[3]);
	static void setDrawingColor(Color color);
	static void setDrawingColor(double rgb[3]);

	static void changeSize2d(GLsizei horizontal, GLsizei vertical);
	static void changeSize3d(GLsizei horizontal, GLsizei vertical);

	static void changeWindowTitle(std::string title);

	static void setKeyboardListenerFunction(void(*function)(unsigned char keyCode, int x, int y));
	static void setMouseListenerFunction(void(*function)(int button, int state, int x, int y));
	static void setMainLoopFunction(void(*function)());
	static void setMouseMotionListenerFunction(void(*function)(int x, int y));
	static void changeMainLoopFunction(void(*function)());
#ifdef light == 1
	static void setLightSpecifiers(void(*function)());
#endif
	static void switchUserInteractionStatus();
	static void userRotatingHandlingStatic(int button, int state, int x, int y);
	static void userRotatingHandlingMotion(int x, int y);

	static void rotateObserverPoint(char direction, double angle, char direction2 = -1, double angle2 = -1);
	static void calculateObserverVectorLength();
	static void calculateAzimuthAndElevation();
	static void calculatePosition();

	
};