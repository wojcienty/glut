#include "stdafx.h"
#include "Drawer.h"

std::mt19937_64 Drawer::randomEngine = std::mt19937_64(clock() * (clock() / 2541));
std::uniform_int_distribution<int> Drawer::colorDist(0, 255);

Color Drawer::randomColor() {
	return Color(colorDist(randomEngine), colorDist(randomEngine), colorDist(randomEngine));
}

using namespace Structures;

void Drawer::drawRectangle(Point2d &p1, Point2d &p2) {
	glBegin(GL_POLYGON);
		glVertex2d(p1.x, p1.y);
		glVertex2d(p2.x, p1.y);
		glVertex2d(p2.x, p2.y);
		glVertex2d(p1.x, p2.y);
	glEnd();
}

void Drawer::drawTriangle(Point2d &p1, Point2d &p2, Point2d &p3) {
	glBegin(GL_TRIANGLES);
		glVertex2d(p1.x, p1.y);
		glVertex2d(p2.x, p2.y);
		glVertex2d(p3.x, p3.y);
	glEnd();
}

void Drawer::drawColorfulTriangle(Point2d &p1, Point2d &p2, Point2d &p3) {
	glBegin(GL_TRIANGLES);
	context->setDrawingColor(randomColor());
	glVertex2d(p1.x, p1.y);
	context->setDrawingColor(randomColor());
	glVertex2d(p2.x, p2.y);
	context->setDrawingColor(randomColor());
	glVertex2d(p3.x, p3.y);
	glEnd();
}

void Drawer::drawRegularPolygon(Point2d & center, double radius, int sides) {
	if (sides < 3) sides = 3;
	if (radius < 0) radius = 1;

	double angle = (360) / (double)sides;

	Point2d p1 = { center.x, center.y + radius };

	glBegin(GL_POLYGON);
	glVertex2d(p1.x, p1.y);
	for (int i = 0; i < sides; i++) {
		p1.rotate(angle);
		glVertex2d(p1.x, p1.y);
	}
	glEnd();
}

void Drawer::drawCustomPolygon(std::vector<Point2d> &points) {
	glBegin(GL_POLYGON);
	for (int i = 0; i < points.size(); i++) {
		glVertex2d(points[i].x, points[i].y);
	}
	glEnd();
}

void Drawer::drawCircle(Point2d & center, double radius) {
	Drawer::drawRegularPolygon(center, radius, 360);
}

//Funckja rysuj�ca pojedy�czy kafelek dywanu
void Drawer::drawSierpinskiTile(Point2d startPoint, double size, double noise) {
	std::uniform_int_distribution<int> intDist(0,255);
	std::uniform_real_distribution<double> real_dist((-size/10) * noise, (size/10) * noise);

	double x, y, factor;
	size /= 3;
	for (double row = 0; row < 3; row++) {
		startPoint.move(0, size);
		for (double col = 0; col < 3; col++) {
			if (row == 1 && col == 1) continue;
			glBegin(GL_POLYGON);
			factor = col * size;
			//p1
			x = startPoint.x + real_dist(randomEngine) + factor;
			y = startPoint.y + real_dist(randomEngine);
			context->setDrawingColor(randomColor());
			glVertex2d(x, y);
			//p2
			x = startPoint.x + real_dist(randomEngine) + factor;
			y = startPoint.y + real_dist(randomEngine) + size;
			context->setDrawingColor(randomColor());
			glVertex2d(x, y);
			//p3
			x = startPoint.x + real_dist(randomEngine) + size + factor;
			y = startPoint.y + real_dist(randomEngine) + size;
			context->setDrawingColor(randomColor());
			glVertex2d(x, y);
			//p4
			x = startPoint.x + real_dist(randomEngine) + size + factor;
			y = startPoint.y + real_dist(randomEngine);
			context->setDrawingColor(randomColor());
			glVertex2d(x, y);

			glEnd();
		}
	}
}

void Drawer::drawColorfulPoint(Point2d point) {
	glBegin(GL_POINTS);
	context->setDrawingColor(randomColor());
	glVertex2d(point.x, point.y);
	glEnd();
}

void Drawer::draw3dAxes(double size) {
	glLineWidth(5);
	glBegin(GL_LINES);
	//X axis
	glColor3d(1, 0, 0);
	glVertex3d(-size, 0, 0);
	glVertex3d(size, 0, 0);
	//Y axis
	glColor3d(0, 1, 0);
	glVertex3d(0, -size, 0);
	glVertex3d(0, size, 0);
	//Z axis
	glColor3d(0, 0, 1);
	glVertex3d(0, 0, -size);
	glVertex3d(0, 0, size);
	glEnd();
	glLineWidth(1);
}

