
#include "stdafx.h"

const Color Color::WHITE = Color(255, 255, 255);
const Color Color::RED = Color(255, 0, 0);
const Color Color::BLACK = Color(0, 0, 0);
const Color Color::BLUE = Color(0, 0, 255 );
const Color Color::YELLOW = Color( 255, 255, 0 );
const Color Color::ORANGE = Color(255, 128, 0);
const Color Color::PURPLE = Color(155, 0,  155 );
const Color Color::PINK = Color(255, 0, 127);




double * Color::toDoubleTab() {
	double output[3] = {this->red, this->green, this->blue};
	return output;
}