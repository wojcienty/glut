#pragma once

#include "stdafx.h"

using namespace Structures;
class Drawer {

private:
	GlutContext * context;
	Color randomColor();
	static std::uniform_int_distribution<int> colorDist;

public:
	static std::mt19937_64 randomEngine;

	void drawRectangle(Point2d &p1, Point2d &p2);
	void drawTriangle(Point2d &p1, Point2d &p2, Point2d &p3);
	void drawColorfulTriangle(Point2d &p1, Point2d &p2, Point2d &p3);
	void drawCircle(Point2d &center, double radius);
	void drawRegularPolygon(Point2d &center, double radius, int sides);
	void drawCustomPolygon(std::vector<Point2d> &points);
	void drawSierpinskiTile(Point2d startPoint, double size, double noise);
	void drawColorfulPoint(Point2d point);
	void draw3dAxes(double size);
	
};

