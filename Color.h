#pragma once

#include "stdafx.h"

class Color {

public:
	double * toDoubleTab();

	static const Color WHITE;
	static const Color RED;
	static const Color BLACK;
	static const Color BLUE;
	static const Color YELLOW;
	static const Color ORANGE;
	static const Color PURPLE;
	static const Color PINK;

	Color(double red, double green, double blue) : red(red), green(green), blue(blue) {}
	Color(int red, int green, int blue) : red(red/ (double)255), green(green / (double)255), blue(blue / (double)255) {}
	Color(int rgb[3]) : red(rgb[0] / (double) 255), green(rgb[1] / (double) 255), blue(rgb[2] / (double) 255) {}
	Color(double rgb[3]) : red(rgb[0]), green(rgb[1]), blue(rgb[2]) {}
	Color() {}

	double red;
	double green;
	double blue;



};
