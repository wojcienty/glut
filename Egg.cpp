#include "stdafx.h"

using namespace Structures;
EggScene::EggScene(Point3d startPoint, double divisor /* = 100 */, double size /* = 1 */) : startPoint(startPoint), divisor(divisor), size(size) {

	#if light == 0
	std::random_device rd;
	std::mt19937 engine(rd());
	std::uniform_real_distribution<double> colorDist(0, 1);
	#endif

	divisor += 1;

	pointsMatrix = PointMatrix(divisor);
	#if light == 1
	normalVectorsMatrix = NormalVectorsMatrix(divisor);
	#else
	colorMatrix = ColorMatrix(divisor);
	#endif // light == 1
	for (int i = 0; i < divisor; i++) {
		pointsMatrix[i] = std::vector<Point3d>(divisor);
		#if light == 1
		normalVectorsMatrix[i] = std::vector<Vector3d>(divisor);
		#else
		colorMatrix[i] = std::vector<Color>(divisor);
		#endif // light == 1
	}
	//Tworzenie macierzy punkt�w wed�ug podanego wzoru
	for (int row = 0; row < divisor; row++) {
		for (int col = 0; col < divisor; col++) {
			double v = col / (divisor - 1);
			double u = row / (divisor - 1);
			double x =
				(-90 * pow(u, 5)
					+ 225 * pow(u, 4)
					- 270 * pow(u, 3)
					+ 180 * pow(u, 2)
					- 45 * (u)) * cos(M_PI * v);
			double y =
				160 * pow(u, 4)
				- 320 * pow(u, 3)
				+ 160 * pow(u, 2);
			double z =
				(-90 * pow(u, 5)
					+ 225 * pow(u, 4)
					- 270 * pow(u, 3)
					+ 180 * pow(u, 2)
					- 45 * (u)) * sin(M_PI * v);
			pointsMatrix[row][col] = Point3d((x * size) + startPoint.x, (y * size) + startPoint.y, (z * size) + startPoint.z);

			#if light == 1
			//Wyliczanie wektor�w wed�ug wzoru
			double x_u = (-450 * pow(u, 4)
				+ 900 * pow(u, 3)
				- 810 * pow(u, 2)
				+ 360 * u
				- 45)
				* cos(M_PI * v);
			double x_v = M_PI * (90 * pow(u, 5)
				- 225 * pow(u, 4)
				+ 270 * pow(u, 3)
				- 180 * pow(u, 2)
				+ 45 * u)
				* sin(M_PI * v);

			double y_u = 640 * pow(u, 3)
				- 960 * pow(u, 2)
				+ 320 * u;
			double y_v = 0;

			double z_u = (-450 * pow(u, 4)
				+ 900 * pow(u, 3)
				- 810 * pow(u, 2)
				+ 360 * u
				- 45)
				* sin(M_PI * v);
			double z_v = -M_PI * (90 * pow(u, 5)
				- 225 * pow(u, 4)
				+ 270 * pow(u, 3)
				- 180 * pow(u, 2)
				+ 45 * u)
				* cos(M_PI * v);

			double nx = y_u * z_v - z_u * y_v;
			double ny = z_u * x_v - x_u * z_v;
			double nz = x_u * y_v - y_u * x_v;

			double lenghtOfNormalVector = sqrt(pow(nx, 2) + pow(ny, 2) + pow(nz, 2));
			nx /= lenghtOfNormalVector;
			ny /= lenghtOfNormalVector;
			nz /= lenghtOfNormalVector;
			if (row == (divisor - 1) || row == 0) {
				normalVectorsMatrix[row][col] = Vector3d(0, -1, 0);
			} else if (row == ((divisor - 1) / 2)) {
				normalVectorsMatrix[row][col] = Vector3d(0, 1, 0);
			} else if (row < ((divisor - 1) / 2)) {
				normalVectorsMatrix[row][col] = Vector3d(nx, ny, nz);
			} else {
				normalVectorsMatrix[row][col] = Vector3d(-nx, -ny, -nz);
			}
			#else
			colorMatrix[row][col] = Color(colorDist(engine), colorDist(engine), colorDist(engine));
			#endif
		}
	}
	divisor -= 1;
}

//Fuckcja obs�uguj�ca scen� g��wn�, w zale�no�ci od wyboru rysuje r�ny wariant jajka
void EggScene::mainScene() {
	switch (this->choice) {
		case '1': {
			drawPoints();
			break;
		}
		case '2': {
			drawMesh();
			break;
		}
		case '3': {
			drawTriangles();
			break;
		}
		default: break;
	}
}

//Fuckcja obs�uguj�ca klawiatur�, obroty i zmiany wariantu rysowania
void EggScene::keyboardHandler(unsigned char keyCode, int x, int y) {
	switch (keyCode) {
		case '1': {
			choice = '1';
			break;
		}
		case '2': {
			choice = '2';
			break;
		}
		case '3': {
			choice = '3';
			break;
		}
		default: break;
	}
}

void EggScene::mouseHandler(int button, int state, int x, int y) {}

//Wariant rysowania punkt�w
void EggScene::drawPoints() {
	glColor3d(1, 1, 1);
	glBegin(GL_POINTS);
	for (int row = 0; row < pointsMatrix.size(); row++) {
		for (int col = 0; col < pointsMatrix.size(); col++) {
			glVertex3dv(pointsMatrix[row][col].toDoubleTab());
		}
	}
	glEnd();
}
//Wariant rysowania siatki tr�j�t�w
void EggScene::drawMesh() {
	Point3d p1, p2, p3, p4;
	glColor3d(1, 1, 1);
	glBegin(GL_LINES);
	for (int row = 0; row < pointsMatrix.size() - 1; row++) {
		for (int col = 0; col < pointsMatrix.size() - 1; col++) {
			p1 = pointsMatrix[row][col];
			p2 = pointsMatrix[row][col + 1];
			p3 = pointsMatrix[row + 1][col + 1];
			p4 = pointsMatrix[row + 1][col];
			drawVertex3d(p1, p2, p3, p4);
		}
	}
	glEnd();
}

//Funckja rysuj�ca przeci�ty prostok�t z podanych punkt�w
void EggScene::drawVertex3d(Point3d p1, Point3d p2, Point3d p3, Point3d p4) {
	glVertex3d(p2.x, p2.y, p2.z);
	glVertex3d(p1.x, p1.y, p1.z);

	glVertex3d(p1.x, p1.y, p1.z);
	glVertex3d(p4.x, p4.y, p4.z);

	glVertex3d(p4.x, p4.y, p4.z);
	glVertex3d(p3.x, p3.y, p3.z);

	glVertex3d(p3.x, p3.y, p3.z);
	glVertex3d(p2.x, p2.y, p2.z);

	glVertex3d(p1.x, p1.y, p1.z);
	glVertex3d(p3.x, p3.y, p3.z);
}

std::random_device rd;
std::mt19937_64 engine(rd());
std::uniform_real_distribution<double> colorDist(0, 1);



#if light == 1
//Funckja rysuj�ca dwa tworz�ce prostok�t, kolorowe tr�jk�ty
void EggScene::drawLitTriangle(Point3d p1, Point3d p2, Point3d p3, Point3d p4, Vector3d v1, Vector3d v2, Vector3d v3, Vector3d v4) {
	glBegin(GL_TRIANGLES);

	glNormal3d(v2.x, v2.y, v2.z);
	glVertex3d(p2.x, p2.y, p2.z);

	glNormal3d(v4.x, v4.y, v4.z);
	glVertex3d(p4.x, p4.y, p4.z);

	glNormal3d(v3.x, v3.y, v3.z);
	glVertex3d(p3.x, p3.y, p3.z);

	glNormal3d(v1.x, v1.y, v1.z);
	glVertex3d(p1.x, p1.y, p1.z);

	glNormal3d(v4.x, v4.y, v4.z);
	glVertex3d(p4.x, p4.y, p4.z);

	glNormal3d(v2.x, v2.y, v2.z);
	glVertex3d(p2.x, p2.y, p2.z);

	glEnd();
}
#else
//Funckja rysuj�ca dwa tworz�ce prostok�t, kolorowe tr�jk�ty
void EggScene::drawColorfulTriangle(Point3d p1, Point3d p2, Point3d p3, Point3d p4, Color c1, Color c2, Color c3, Color c4) {
	glBegin(GL_TRIANGLES);
	glColor3d(c2.red, c2.green, c2.blue);
	glVertex3d(p2.x, p2.y, p2.z);

	glColor3d(c4.red, c4.green, c4.blue);
	glVertex3d(p4.x, p4.y, p4.z);

	glColor3d(c3.red, c3.green, c3.blue);
	glVertex3d(p3.x, p3.y, p3.z);

	glColor3d(c1.red, c1.green, c1.blue);
	glVertex3d(p1.x, p1.y, p1.z);

	glColor3d(c4.red, c4.green, c4.blue);
	glVertex3d(p4.x, p4.y, p4.z);

	glColor3d(c2.red, c2.green, c2.blue);
	glVertex3d(p2.x, p2.y, p2.z);
	glEnd();
}
#endif // light == 1


//Wariant rysowania tr�j�t�w
void EggScene::drawTriangles() {

	glColor3d(1, 1, 1);
	Point3d p1, p2, p3, p4;
	#if light == 1
	Vector3d v1, v2, v3, v4;
	#else
	Color c1, c2, c3, c4;
	#endif
	int row = 0;
	for (row = 0; row < divisor; row++) {
		for (int col = 0; col < divisor; col++) {
			p1 = pointsMatrix[row][col];
			p2 = pointsMatrix[row][col + 1];
			p3 = pointsMatrix[row + 1][col + 1];
			p4 = pointsMatrix[row + 1][col];

			#if light == 1
			v1 = normalVectorsMatrix[row][col];
			v2 = normalVectorsMatrix[row][col + 1];
			v3 = normalVectorsMatrix[row + 1][col + 1];
			v4 = normalVectorsMatrix[row + 1][col];
			drawLitTriangle(p1, p2, p3, p4, v1, v2, v3, v4);
			#else
			c1 = colorMatrix[row][col];
			c2 = colorMatrix[row][col + 1];
			c3 = colorMatrix[row + 1][col + 1];
			c4 = colorMatrix[row + 1][col];
			drawColorfulTriangle(p1, p2, p3, p4, c1, c2, c3, c4);
			#endif // light == 1
		}
	}
	glColor3d(1, 1, 1);
}

//Niedoko�czona funckja rysowania polygon�w
void EggScene::drawPolygons() {
	glColor3d(1, 1, 1);
	glBegin(GL_POINTS);
	glEnd();
}
#if light == 1
void EggScene::setLightParameters() {
	// Ustawienie patrametr�w materia�u
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glMaterialfv(GL_FRONT, GL_SPECULAR, this->materialSpecular);
	glMaterialfv(GL_FRONT, GL_AMBIENT, this->materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, this->materialDiffuse);
	glMaterialf(GL_FRONT, GL_SHININESS, this->materialShininess);

	// Ustawienie parametr�w �r�d�a nr 0
	glLightfv(GL_LIGHT0, GL_AMBIENT, this->firstLightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, this->firstLightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, this->firstLightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, this->firstLightPosition);

	glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, this->attributeConstant);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, this->attributeLinear);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, this->attributeQuadratic);

	// Ustawienie parametr�w �r�d�a nr 1
	glLightfv(GL_LIGHT1, GL_AMBIENT, this->secondLightAmbient); 
	glLightfv(GL_LIGHT1, GL_DIFFUSE, this->secondLightDiffuse); 
	glLightfv(GL_LIGHT1, GL_SPECULAR, this->secondLightSpecular); 
	glLightfv(GL_LIGHT1, GL_POSITION, this->secondLightPosition);

	glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, this->attributeConstant);
	glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, this->attributeLinear);
	glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, this->attributeQuadratic);

	// Ustawienie opcji systemu o�wietlania sceny
	glShadeModel(GL_SMOOTH); // w�aczenie �agodnego cieniowania
	glEnable(GL_LIGHTING);   // w�aczenie systemu o�wietlenia sceny
	glEnable(GL_LIGHT0);     // w��czenie �r�d�a o numerze 0
	glEnable(GL_LIGHT1);     // w��czenie �r�d�a o numerze 1
}


void EggScene::userInteractionHandlingStatic(int button, int state, int x, int y) {
	//Funckja wykrywaj�ca przyci�ni�cie klawiszy myszy
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		leftMouseButtonPressed = true;
	} else {
		leftMouseButtonPressed = false;
	}

	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		rightMouseButtonPressed = true;
	} else {
		rightMouseButtonPressed = false;
	}

	previousMouseX = actualMouseX;
	previousMouseY = actualMouseY;
	actualMouseX = x;
	actualMouseY = y;
}

void EggScene::updateAngle(float &angle, float delta) {
	angle += delta;

	if (angle < 0) {
		angle += 2 * M_PI;
	}

	if (angle > 2 * M_PI) {
		angle = angle - 2 * M_PI;
	}
}

void EggScene::userInteractionHandlingMotion(int x, int y) {
	//Funckja reaguj�ca na przesuni�cie myszy na ekranie
	if (!userInteractionStatus) {
		return;
	}
	previousMouseX = actualMouseX;
	previousMouseY = actualMouseY;
	actualMouseX = x;
	actualMouseY = y;

	//Je�eli kt�ry� z przycisk�w zosta� wci�ni�ty, zostaj� wykonane odpowiednie akcje
	float deltaX = previousMouseX - actualMouseX;
	float deltaY = previousMouseY - actualMouseY;
	if (leftMouseButtonPressed) {
		double vectorLenght = sqrt(pow(this->firstLightPosition[0], 2) + pow(this->firstLightPosition[1], 2) + pow(this->firstLightPosition[2], 2));
		updateAngle(firstLightAzimuth, (previousMouseX - actualMouseX) / 100);
		updateAngle(firstLightElevation, (previousMouseY - actualMouseY) / 100);

		this->firstLightPosition[0] = vectorLenght * cos(firstLightAzimuth) * cos(firstLightElevation);
		this->firstLightPosition[1] = vectorLenght * sin(firstLightElevation);
		this->firstLightPosition[2] = vectorLenght * sin(firstLightAzimuth) * cos(firstLightElevation);

		glLightfv(GL_LIGHT0, GL_POSITION, this->firstLightPosition);
	} else if (rightMouseButtonPressed) { 
		double vectorLenght = sqrt(pow(this->secondLightPosition[0], 2) + pow(this->secondLightPosition[1], 2) + pow(this->secondLightPosition[2], 2));
		updateAngle(secondLightAzimuth, (previousMouseX - actualMouseX) / 100);
		updateAngle(secondLightElevation,  (previousMouseY - actualMouseY) / 100);

		this->secondLightPosition[0] = vectorLenght * cos(secondLightAzimuth) * cos(secondLightElevation);
		this->secondLightPosition[1] = vectorLenght * sin(secondLightElevation);
		this->secondLightPosition[2] = vectorLenght * sin(secondLightAzimuth) * cos(secondLightElevation);
		glLightfv(GL_LIGHT1, GL_POSITION, this->secondLightPosition);
	}

}

void EggScene::switchUserInteractionStatus() {
	//zmiana statusu obs�ugi u�ytkownika
	userInteractionStatus = !userInteractionStatus;
}
















#endif