
#include "stdafx.h"

//Funckja wywo�ywana w rekurencji
void Scenes::drawCarpet(Structures::Point2d startPoint, double size, int steps, double noise) {
	if (steps == 0) {
		return;
	} else {
		--steps;
		if (steps == 0) {
			//Funckja rysuj�ca pojedy�czy "kafelek" dywanu sierpi�skiego
			this->drawer->drawSierpinskiTile(startPoint, size, noise);
		}

		double thirdPart = size / 3;
		drawCarpet(startPoint, thirdPart, steps, noise);

		startPoint.move(thirdPart, 0);
		drawCarpet(startPoint, thirdPart, steps, noise);

		startPoint.move(thirdPart, 0);
		drawCarpet(startPoint, thirdPart, steps, noise);
		
		startPoint.move(0, thirdPart);
		drawCarpet(startPoint, thirdPart, steps, noise);

		startPoint.move(0, thirdPart);
		drawCarpet(startPoint, thirdPart, steps, noise);

		startPoint.move(-thirdPart, 0);
		drawCarpet(startPoint, thirdPart, steps, noise);

		startPoint.move(-thirdPart, 0);
		drawCarpet(startPoint, thirdPart, steps, noise);

		startPoint.move(0, -thirdPart);
		drawCarpet(startPoint, thirdPart, steps, noise);
	}
}

//Fukcja rysowania sceny
void Scenes::sierpinskiCarpet(Structures::Point2d &startPoint, double size, int steps, double noise) {
	this->context->clear();
	drawCarpet(startPoint, size, steps, noise);
	this->context->flush();
}

void Scenes::drawTriangle(Point2d a, double size, int step) {

	if (step == 0) {
		return;
	} else {
		--step;
		double secondPart = size / 2;
		if (step == 0) {
			Point2d b = Point2d(a.x + secondPart, a.y + size);
			Point2d c = Point2d(a.x + size, a.y);
			this->drawer->drawColorfulTriangle(a, b, c);
		}

		drawTriangle(a, secondPart, step);
		a.move(secondPart / 2, secondPart);
		drawTriangle(a, secondPart, step);
		a.move(secondPart / 2, -secondPart);
		drawTriangle(a, secondPart, step);
	}

}

void Scenes::sierpinskiTriangle_rec(Point2d &a, double size, int steps) {
	this->context->clear();
	drawTriangle(a, size, steps);
	this->context->flush();
}


bool Scenes::pointInTriangle(Point2d &triangleStartPoint, double triangleSize, Point2d &testedPoint) {

	Point2d a, b, c;
	a = triangleStartPoint;
	b = Point2d(a.x + triangleSize / 2, a.y + triangleSize);
	c = Point2d(a.x + triangleSize, a.y);

	//r�wnanie prostej  => (x2 - x1)(y - y1) = (y2 - y1)(x - x1)
	//punkt musi si� mie�ci� w trzech naszych

	//prosta ab
	bool firstCriterion = ((b.x - a.x)*(testedPoint.y - a.y) - (b.y - a.y)*(testedPoint.x - a.x)) <= 0;
	if (!firstCriterion) {
		return false;
	}
	//prosta bc
	bool secondCriterion = ((c.x - b.x)*(testedPoint.y - b.y) - (c.y - b.y)*(testedPoint.x - b.x)) <= 0;
	if (!secondCriterion) {
		return false;
	}
	//prosta ac
	bool thirdCriterion = ((c.x - a.x)*(testedPoint.y - a.y) - (c.y - a.y)*(testedPoint.x - a.x)) >= 0;
	if (!thirdCriterion) {
		return false;
	}

	return true;
}


void Scenes::sierpinskiTriangle_chaos(Point2d &triangleStartPoint, double size, unsigned long long diceRolls) {
	this->context->clear();
	typedef unsigned long long ull;

	std::uniform_int_distribution<int> rollDice(0,2);
	std::uniform_real_distribution<double> pointDist(0, size); //�eby domkn�� przedzia� :)
	int dice = rollDice(Drawer::randomEngine);

	//wierzcho�ki tr�jk�ta
	Point2d a, b, c;
	a = triangleStartPoint;
	b = Point2d(a.x + size / 2, a.y + size);
	c = Point2d(a.x + size, a.y);

	Point2d vertex[3] = {a, b, c};
	Point2d randomPoint;

	//losujemy punkt z wn�trza tr�jk�ta
	do {
		randomPoint = {triangleStartPoint.x + pointDist(Drawer::randomEngine), triangleStartPoint.y + pointDist(Drawer::randomEngine)};
	} while (!pointInTriangle(a, size, randomPoint));

	for (ull i = 0; i < diceRolls; ++i) {
		dice = rollDice(Drawer::randomEngine);
		randomPoint.x = (randomPoint.x + vertex[dice].x) / 2.0;
		randomPoint.y = (randomPoint.y + vertex[dice].y) / 2.0;
		this->drawer->drawColorfulPoint(randomPoint);
	}

	this->context->flush();
}